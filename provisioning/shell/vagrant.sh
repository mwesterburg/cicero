#!/bin/bash

# Paths
bash_profile_path=/home/vagrant/.bash_profile
share_origin_path=/vagrant
share_destination_path=/home/vagrant/share
vagrant_home=/home/vagrant

# Options
interactive=false
while getopts ":i:" opt; do
    case $opt in
        i)
            interactive=true
            ;;
    esac
done

if [ -f $bash_profile_path ]; then
    rm $bash_profile_path
fi

touch $bash_profile_path
chown vagrant:vagrant $bash_profile_path

cat <<EOT >> $bash_profile_path
export PATH=$PATH:/vagrant/tools:/home/vagrant/ansible/bin
EOT
source $bash_profile_path

apt-get update -y
apt-get install -y git python-yaml python-jinja2 python-pycurl
if [ -d $vagrant_home/ansible/ ]; then
    rm -rf $vagrant_home/ansible/
fi
git clone https://github.com/ansible/ansible.git $vagrant_home/ansible/
chown -R vagrant:vagrant /home/vagrant/ansible
cd $vagrant_home/ansible/
source ./hacking/env-setup

if [ ! -d $share_destination_path ]; then
    ln -s $share_origin_path $share_destination_path
fi

provision django-dev
